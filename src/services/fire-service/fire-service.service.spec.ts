import { Test, TestingModule } from '@nestjs/testing';
import { FireServiceService } from './fire-service.service';

describe('FireServiceService', () => {
  let service: FireServiceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FireServiceService],
    }).compile();

    service = module.get<FireServiceService>(FireServiceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
