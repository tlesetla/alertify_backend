import { FireServiceService } from './../../services/fire-service/fire-service.service';
import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
@Controller('fire')
export class FireController {

    constructor(private fireService: FireServiceService) {}

    @Get()
    findAll() {
       // return this.fireService.findAll();
    }

    @Get('live')
    findLive() {
        // return this.fireService.findLive();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        // return this.fireService.findOne(id);
    }

    @Post()
    create(@Body() sensorData: any) {
        console.log(sensorData);
        // this.fireService.create(sensorData);
    }

    @Put()
    update(@Param('id') id: string) {
        return 'update';
    }

    @Delete(':id')
    delete() {
        return 'delete';
    }
}
