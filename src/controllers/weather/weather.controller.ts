import { WeatherServiceService } from './../../services/weather-service/weather-service.service';
import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';

@Controller('weather')
export class WeatherController {

    constructor(private weatherService: WeatherServiceService) {}
    @Get()
    findAll() {
       // return this.weatherService.findAll();
    }

    @Get('live')
    findLive() {
        // return this.weatherService.findLive();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        // return this.weatherService.findOne(id);
    }

    @Post()
    create(@Body() sensorData: any) {
        console.log(sensorData);
        // this.weatherService.create(sensorData);
    }

    @Put(':id')
    update(@Param('id') id: string) {
        return 'update';
    }

    @Delete(':id')
    delete() {
        return 'delete';
    }
}
