import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FireController } from './controllers/fire/fire.controller';
import { WeatherController } from './controllers/weather/weather.controller';
import { FireServiceService } from './services/fire-service/fire-service.service';
import { WeatherServiceService } from './services/weather-service/weather-service.service';

@Module({
  imports: [],
  controllers: [AppController, FireController, WeatherController],
  providers: [AppService, FireServiceService, WeatherServiceService],
})
export class AppModule {}
